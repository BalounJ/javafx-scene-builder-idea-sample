package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/*
*   Controller
*
*   v sample.fxml potreba:
*
*   fx:controller="sample.Controller"
*   - pripojeni Controller
*
*   fx:id="btn"
*   - vsem manipulovatelnym prvkum nastavit fx:id
*
*   public Button btn;
*   - pridat manipulovatelne objekty
*   - lze v fxml: alt + enter
*
*
*
* */

public class Controller {
    public Button btn;
    public TextArea area;
    public TextField input;
    public Button btn2;
    public Label lbl;

    public void btnClicked(ActionEvent actionEvent) {
        System.out.println(actionEvent.toString());
        if(actionEvent.getSource() == btn) {
            area.setText(area.getText() + System.lineSeparator() + input.getText());
        }
    }
}
